package friscilla.lely.uts_android


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener{
    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener {
            fbAuth.signOut()
        }
        btndtktg.setOnClickListener(this)
        btnktg.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btndtktg->{
                val intent = Intent(this,DatakatalogActivity::class.java)
                startActivity(intent)
            }
            R.id.btnktg->{
                val intent = Intent(this, OutletActivity::class.java)
                startActivity(intent)
            }
        }
    }

}
